CC = g++
FLAGS = -fopenmp
O = -O0
STD = c++14

main: main.o
	$(CC) -o main main.o $(FLAGS) $(O) -std=$(STD)
 
main.o: main.cpp
	$(CC) -c -g -Wall $(FLAGS) $(O) main.cpp -std=$(STD)

clean:
	rm -f *.o main
	
format:
	clang-format main.cpp -i