#include <cstdlib>
#include <ctime>
#include <iostream>
#include <omp.h>
#include <vector>

using namespace std;

#define WIDTH = 5
#define HEIGHT = 5

double get_random_number(double min, double max);
vector<vector<double>> get_random_vector(int tam);
void print_vector(vector<vector<double>> array);
vector<vector<double>> sum_vectors(vector<vector<double>> first,
                                   vector<vector<double>> second);

int main() {
  srand(time(NULL));
  vector<vector<double>> first, second, sum;

  first = get_random_vector(5);
  second = get_random_vector(5);
  sum = sum_vectors(first, second);

  cout << "The first array is:" << endl;
  print_vector(first);
  cout << "The second array is:" << endl;
  print_vector(second);
  cout << "The sum of both arrays is:" << endl;
  print_vector(sum);
}

vector<vector<double>> sum_vectors(vector<vector<double>> first,
                                   vector<vector<double>> second) {
  int tam = first.size();
  vector<vector<double>> result;
  result.resize(tam);
  for (int i = 0; i < tam; ++i) {
    result[i].resize(tam);
  }

#pragma omp parallel for
  for (int i = 0; i < tam; ++i) {
#pragma omp parallel for
    for (int j = 0; j < tam; ++j) {
      result[i][j] = first[i][j] + second[i][j];
    }
  }

  return result;
}

vector<vector<double>> get_random_vector(int tam) {
  vector<vector<double>> array;
  array.resize(tam);
  for (int i = 0; i < tam; ++i) {
    array[i].resize(tam);
  }

  for (int i = 0; i < array.size(); ++i) {
    for (int j = 0; j < array[i].size(); ++j) {
      array[i][j] = get_random_number(1.0, 100.0);
    }
  }

  return array;
}

void print_vector(vector<vector<double>> array) {
  for (int i = 0; i < array.size(); ++i) {
    for (int j = 0; j < array[i].size(); ++j) {
      cout << array[i][j] << " | ";
      if (j == array[i].size() - 1) {
        cout << endl;
      }
    }
  }
}

double get_random_number(double min, double max) {
  double random = (double)rand() / RAND_MAX;
  random = min + random * (max - min);
  return random;
}
